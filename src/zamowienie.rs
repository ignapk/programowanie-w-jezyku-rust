use crate::pozycja::PozycjaZamowienia;

#[derive(Debug)]
pub struct Zamowienie {
    zamowienia: Vec<PozycjaZamowienia>,
}

impl Zamowienie {
    pub fn new() -> Self {
        Zamowienie { zamowienia: Vec::new() }
    }
    pub fn dodaj_pozycje(&mut self, pozycja: PozycjaZamowienia) {
        self.zamowienia.push(pozycja);
    }
    pub fn razem(&self) -> f64 {
        self.zamowienia.iter().map(|p| p.zwroc_wartosc()).sum()
    }
    pub fn zawartosc(&self, numer: usize) -> &PozycjaZamowienia {
        self.zamowienia.get(numer).unwrap()
    }
    pub fn usun(&mut self, numer: usize) {
        self.zamowienia.remove(numer);
    }
    pub fn zmien(&mut self, numer: usize, liczba: i32) {
        self.zamowienia[numer].zmien_liczbe_sztuk(liczba);
    }
}

#[cfg(test)]
mod tests {
    use crate::towar::Towar;
    use crate::pozycja::PozycjaZamowienia;
    use super::Zamowienie;

    #[test]
    fn can_add() {
        let t = Towar::new(String::from("Kij"), 2.50);
        let p = PozycjaZamowienia::new(t, 4);
        let mut z = Zamowienie::new();
        z.dodaj_pozycje(p);
        assert_eq!(z.zawartosc(0).ile_sztuk(), 4);
    }

    #[test]
    fn can_check_number() {
        let t1 = Towar::new(String::from("Kij"), 2.50);
        let t2 = Towar::new(String::from("Klucz"), 3.50);
        let p1 = PozycjaZamowienia::new(t1, 4);
        let p2 = PozycjaZamowienia::new(t2, 3);
        let mut z = Zamowienie::new();
        z.dodaj_pozycje(p1);
        z.dodaj_pozycje(p2);
        assert_eq!(z.razem(), 20.5);
    }
}
