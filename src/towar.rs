#[derive(Debug)]
pub struct Towar {
    nazwa: String,
    cena: f64,
}

impl Towar {
    pub fn new(nazwa: String, cena: f64) -> Self {
        Towar { nazwa, cena }
    }
    pub fn zwroc_nazwe(&self) -> &str {
        &self.nazwa
    }
    pub fn zwroc_cene(&self) -> f64 {
        self.cena
    }
}

#[cfg(test)]
mod tests {
    use super::Towar;

    #[test]
    fn can_create() {
        let t = Towar::new(String::from("Drukarka"), 699.99);
        assert_eq!(t.zwroc_nazwe(), "Drukarka");
        assert_eq!(t.zwroc_cene(), 699.99);
    }
}
