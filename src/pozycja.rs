use crate::towar::Towar;

#[derive(Debug)]
pub struct PozycjaZamowienia {
    towar: Towar,
    liczba: i32,
}

impl PozycjaZamowienia {
    pub fn new(towar: Towar, liczba: i32) -> Self {
        PozycjaZamowienia { towar, liczba }
    }
    pub fn zwroc_towar(&self) -> &Towar {
        &self.towar
    }
    pub fn ile_sztuk(&self) -> i32 {
        self.liczba
    }
    pub fn zwroc_wartosc(&self) -> f64 {
        self.ile_sztuk() as f64 * self.towar.zwroc_cene()
    }
    pub fn zmien_liczbe_sztuk(&mut self, liczba: i32) {
        self.liczba = liczba;
    }
}

#[cfg(test)]
mod tests {
    use super::PozycjaZamowienia;
    use crate::towar::Towar;

    #[test]
    fn can_create() {
        let t = Towar::new(String::from("Mysz"), 19.99);
        let p = PozycjaZamowienia::new(t, 2);
        assert_eq!(p.zwroc_towar().zwroc_nazwe(), "Mysz");
        assert_eq!(p.zwroc_towar().zwroc_cene(), 19.99);
        assert_eq!(p.ile_sztuk(), 2);
    }

    #[test]
    fn can_sum() {
        let t = Towar::new(String::from("Mysz"), 19.99);
        let p = PozycjaZamowienia::new(t, 2);
        assert_eq!(p.zwroc_wartosc(), 2.0 * 19.99);
    }

    #[test]
    fn can_change_number() {
        let t = Towar::new(String::from("Mysz"), 19.99);
        let mut p = PozycjaZamowienia::new(t, 2);
        p.zmien_liczbe_sztuk(3);
        assert_eq!(p.ile_sztuk(), 3);
    }
}
